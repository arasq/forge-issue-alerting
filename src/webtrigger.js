import { ALERTS_STORAGE_LIMIT } from './constants';
import api, { storage } from "@forge/api";

const buildOutput = (body = '', statusCode = 200, statusText = 'OK') => ({
    body,
    headers: {
        'Content-Type': ['application/json'],
    },
    statusCode,
    statusText,
});

export default async (req) => {
    const alerts = await storage.get('alert');
    const newAlert = JSON.parse(req.body);

    const currentAlerts = [newAlert, ...(alerts ? alerts : [])];

    if (currentAlerts.length > ALERTS_STORAGE_LIMIT) {
        currentAlerts.length = ALERTS_STORAGE_LIMIT;
    }

    await storage.set('alert', currentAlerts);

    return buildOutput();
};