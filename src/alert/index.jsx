import ForgeUI, { Fragment, Image, Text, StatusLozenge } from "@forge/ui";
import { getAlertStatus, getStatusEmoji, getStatusAppearance } from '../utils';

export default ({ data, withSeparator = false, asLatest = false }) => {
    const { imageUrl, severity, description, detectorUrl, timestamp, status } = data;
    return (
        <Fragment>
            {
                asLatest && <Text content={"__Latest Alert:__"} />
            }
            <Text>
                {getStatusEmoji(getAlertStatus(status))} *{new Date(timestamp).toLocaleString()}*
                {' '}
                <StatusLozenge text={severity} appearance={getStatusAppearance(severity)} /> 
            </Text>
            <Text content={description} />
            <Image src={imageUrl} />
            <Text content={`[See alert in SignalFx](${detectorUrl})`} />
            {
                withSeparator && <Text content={"-"} />
            }
        </Fragment>
    );
}