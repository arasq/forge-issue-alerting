import ForgeUI, {
    Button,
    ButtonSet,
    Fragment,
    Text,
    ModalDialog,
    render,
    useProductContext,
    useState,
    IssuePanel,
    IssuePanelAction,
    Form,
    TextField,
} from "@forge/ui";
import { useIssueProperty } from '@forge/ui-jira';
import {
    onlyAlertsContainingKeyword,
    getAlertStatus,
    getShowMoreButtonText,
    getStatusEmoji,
    getStatusMessage
} from './utils';
import { ALERT_STATUS } from './constants';
import AlertsFeed from "./alerts-feed";
import Alert from "./alert";
import { storage } from "@forge/api";

const App = () => {
    const WEBTRIGGER_URL = process.env.WEBTRIGGER_URL || `The webtrigger URL is not set. See the app README.`;
    const { extensionContext: { isNewToIssue }, localId } = useProductContext();

    const [alerts] = useState(storage.get('alert'), []);
    const [isConfigured, setIsConfigured] = useState(false);
    const [showFilterDialog, setShowFilterDialog] = useState(false);
    const [showWebhookUrl, setShowWebhookUrl] = useState(false);
    const [showAlertsFeed, setShowAlertsFeed] = useState(false);
    const [feedLength, setFeedLength] = useState(3);

    const [alertFilter, setAlertFilter] = useIssueProperty(`alertFilter`, {});

    const issueAlerts = onlyAlertsContainingKeyword(alerts, alertFilter[localId]);
    const latestAlert = issueAlerts[0] ? issueAlerts[0] : {};
    const latestAlertStatus = issueAlerts.length ?
        getAlertStatus(latestAlert.status) : ALERT_STATUS.NO_ALERTS;

    const updateAlertFilter = async (filter) => {
        const newAlertFilter = Object.assign({}, alertFilter, { [localId]: filter });
        await setAlertFilter(newAlertFilter)
    }

    const hideFeed = () => {
        setShowAlertsFeed(false);
        setFeedLength(3);
    };

    const handleOnSubmit = async (data) => {
        await updateAlertFilter(data.filter);
        setShowFilterDialog(false);
        if (isNewToIssue) {
            setIsConfigured(true);
        }
    }

    const renderApp = () => {
        if ((isNewToIssue && !isConfigured) || showFilterDialog) {
            return (
                <ModalDialog header="Filter" onClose={() => setShowFilterDialog(false)}>
                    <Form onSubmit={handleOnSubmit}>
                        <TextField name="filter" label="Set filter for alerts" />
                    </Form>
                </ModalDialog>
            )
        }

        return (
            <Fragment>
                {
                    alertFilter[localId] && <Text content={`Alerts filtered by: *${alertFilter[localId]}*`} />
                }
                <Text content={`${getStatusEmoji(latestAlertStatus)} __${getStatusMessage(latestAlertStatus)}__`} />
                {
                    latestAlertStatus === ALERT_STATUS.WARNING &&
                    <Alert data={latestAlert} />
                }
                {
                    (issueAlerts.length !== 0 && !showAlertsFeed) &&
                    <Button text="🗓️ Alerts feed" onClick={() => setShowAlertsFeed(true)} />
                }
                {
                    showAlertsFeed &&
                    (
                        <Fragment>
                            <AlertsFeed
                                alerts={
                                    issueAlerts.slice(0, feedLength)
                                }
                                feedLength={feedLength}
                            />
                            <ButtonSet>
                                {
                                    issueAlerts.length > feedLength &&
                                    <Button
                                        text={getShowMoreButtonText(issueAlerts.length, feedLength)}
                                        onClick={() => setFeedLength(feedLength + 5)}
                                    />
                                }
                                <Button
                                    text="Close feed"
                                    onClick={hideFeed}
                                />
                            </ButtonSet>
                        </Fragment>
                    )
                }
                {
                    showWebhookUrl &&
                    <ModalDialog header="Webtrigger URL" onClose={() => setShowWebhookUrl(false)}>
                        <Text content={WEBTRIGGER_URL} />
                    </ModalDialog>
                }
            </Fragment>
        )
    }

    return (
        <IssuePanel actions={[
            <IssuePanelAction text="🔍 Change filter" onClick={() => setShowFilterDialog(true)} />,
            <IssuePanelAction text="🔗 Show webtrigger URL" onClick={() => setShowWebhookUrl(true)} />,
        ]}>
            {renderApp()}
        </IssuePanel>
    );
};

export default render(
    <App />
);