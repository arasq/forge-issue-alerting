import ForgeUI, { Fragment } from "@forge/ui";
import Alert from "../alert/index";

const isLastVisibleAlert = (feedLength, alertsLength, idx) => {
    const isLastAtAll = feedLength >= alertsLength && (idx + 1) === alertsLength;
    const isLastInFeed = feedLength < alertsLength && (idx + 1) === feedLength;
    return isLastInFeed || isLastAtAll;
}

export default ({ alerts, feedLength }) => (
    <Fragment>
        {
            alerts
                .map((alert, idx) => {
                    if (idx === 0) {
                        return <Alert data={alert} asLatest withSeparator />
                    }
                    return isLastVisibleAlert(feedLength, alerts.length, idx) ?
                        (<Alert data={alert} />) :
                        (<Alert data={alert} withSeparator />)
                }
                )
        }
    </Fragment>
);