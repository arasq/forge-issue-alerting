import { ALERT_STATUS, SEVERITY } from './constants';

export const onlyAlertsContainingKeyword = (alerts, keyword = '') =>
    onlyAlertsContainingKeywordInSelectedFields(alerts, keyword,
        'rule',
        'description',
        'tip',
        'detector',
        'messageBody'
    );

export const onlyAlertsContainingKeywordInSelectedFields = (alerts = [], keyword, ...fields) =>
    alerts.filter(alert => fields.find(field => alert[field] && alert[field].includes(keyword)));

export const getAlertStatus = status => {
    switch (status) {
        case "anomalous":
            return ALERT_STATUS.WARNING;
        case "ok":
            return ALERT_STATUS.OK;
        case "manually resolved":
            return ALERT_STATUS.RESOLVED;
        case "stopped":
            return ALERT_STATUS.STOPPED
        default:
            return ALERT_STATUS.NO_ALERTS;
    }
}

export const getStatusEmoji = status => {
    switch (status) {
        case ALERT_STATUS.OK:
            return '✅';
        case ALERT_STATUS.STOPPED:
            return '🛑';
        case ALERT_STATUS.RESOLVED:
            return '👌';
        case ALERT_STATUS.NO_ALERTS:
            return '💤';
        case ALERT_STATUS.WARNING:
            return '⚠️';
        default:
            return '';
    }
}

export const getStatusMessage = status => {
    switch (status) {
        case ALERT_STATUS.OK:
            return 'There is no active alert';
        case ALERT_STATUS.STOPPED:
            return 'Last alert stopped';
        case ALERT_STATUS.RESOLVED:
            return 'Alert resolved manually';
        case ALERT_STATUS.NO_ALERTS:
            return 'There were no alerts yet';
        case ALERT_STATUS.WARNING:
            return 'There is active alert';
        default:
            return 'There is no status data';
    }
}

export const getShowMoreButtonText = (alertsLength, feedLength) => `
  Show ${alertsLength - (feedLength + 5) < 0 ? alertsLength - feedLength : `5`} 
  more ${alertsLength - (feedLength + 5) > 0 ? `(${alertsLength - feedLength} hidden)` : ``}
`; 

export const getStatusAppearance = (severity) => {
    switch (severity) {
        case SEVERITY.CRITICAL:
            return 'removed';
        case SEVERITY.MAJOR:
            return 'removed';
        case SEVERITY.MINOR:
            return 'new';
        case SEVERITY.WARNING:
            return 'moved';
        case SEVERITY.INFO:
            return 'inprogress';
        default:
            return 'default';
    }
}