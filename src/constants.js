export const ALERTS_STORAGE_LIMIT = 20;

export const ALERT_STATUS = {
    WARNING: 'warining',
    OK: 'ok',
    RESOLVED: 'resolved',
    STOPPED: 'stopped',
    NO_ALERTS: 'no alerts',
};

export const SEVERITY = {
    CRITICAL: 'Critical',
    MAJOR: 'Major',
    MINOR: 'Minor',
    WARNING: 'Warning',
    INFO: 'Info',
};