# Issue Alerting

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository
2. Run `forge register` to register a new copy of this app to your developer account
3. Run `npm install` to install your dependencies
4. Run `forge deploy` to deploy the app into the default environment
5. Run `forge install` and follow the prompts to install the app

## Description

This Forge app links Jira issues to SignalFx alerts using webhooks and existing Jira issues.
This feed shows SignalFx alert details, detector charts, and links to view the alert in SignalFx, as filtered by the provided text.

![Issue Alerting in action](docs/images/issue-panel.gif)

## Installation

If this is your first time using Forge, the
[getting started](https://developer.atlassian.com/platform/forge/set-up-forge/)
guide will help you install the prerequisites.

If you have already set up your Forge environment, you can deploy this example straight away.
Visit the [example apps](https://developer.atlassian.com/platform/forge/example-apps/)
page for installation steps.

## Usage

### Configure the environment variables

This Forge app uses
[environment variables](https://developer.atlassian.com/platform/forge/environments/)
for configuration. You'll need to configure these values before using the app.

#### Webtrigger URL

SignalFx detectors are connected to Jira using a Forge
[webtrigger](https://developer.atlassian.com/platform/forge/web-trigger-events-reference/).
This is stored in the `WEBTRIGGER_URL` environment variable to make accessing the
URL quick and easy within the Jira interface.

Webtrigger URLs are unique to each Forge installation. You'll need to rerun these steps
for each new site or when making changes to the `webtrigger` section in `mainifest.yml`.

To set the webtrigger variable:

1. Get the Forge installation IDs for the app by running.

    ```
    forge install:list
    ```

1. Get the webtrigger URL using the *Jira* installation ID from step 1 by running.

    ```
    forge webtrigger <jira-installation-id>
    ```

1. Set the environment variable by running.

    ```
    forge variables:set WEBTRIGGER_URL "<webtrigger-url>"
    ```

1. Redeploy the app to use the environment variable.

    ```
    forge deploy
    ```

If the `WEBTRIGGER_URL` is not set, users clicking the *Copy webtrigger URL* button
will see a warning `The webtrigger URL is not set. See the app README.`

### SignalFx detector configuration

The integration can be configured with new or existing SignalFx detectors. Only
new alerts fired after you've configured the new recipient will be visible in the feed.

See the SignalFx documentation on
[setting up a detector](https://docs.signalfx.com/en/latest/detect-alert/set-up-detectors.html)
if you're not familiar with SignalFx.

To configure the integration for a detector:

1. Edit the SignalFx detector.
1. Click through *Alert recipients > Add Recipient > Webhook > Custom* to open the
*webhook notification* screen.
1. Set the *URL* to the webtrigger URL. You can get this value easily by visiting your
issue in Jira and using the *Show webhook URL* custom action.

See [Send notifications via a webhook URL](https://docs.signalfx.com/en/latest/admin-guide/integrate-notifications.html#webhook)
for more information on SignalFx's webhook payload.

### Multiple instances and configuration

This Forge app uses the [`allowMultiple`](https://developer.atlassian.com/platform/forge/manifest-reference/modules/#jira-issue-panel) property to provide possibility to add multi instance of the same panel and [`isNewToIssue`](https://developer.atlassian.com/platform/forge/ui-kit-components/issue-panel/) context variable to display configuration modal.

![Issue Alerting in action](docs/images/multi-instances.gif)

### Custom actions

This Forge app uses [`IssuePanelAction`](https://developer.atlassian.com/platform/forge/ui-kit-components/issue-panel-action) to add actions to issue panel meatball menu.

![Issue Alerting in action](docs/images/custom-actions.gif)

## Documentation

### Storage

The Forge app uses the [`App storage API`](https://developer.atlassian.com/platform/forge/js-api-reference/storage-api/) to store alerts data. It is limited to 20 entries because of the limits of the API.

### Architecture

The app's [manifest.yml](./manifest.yml) contains three modules:

A [jira:issuePanel module](https://developer.atlassian.com/platform/forge/manifest-reference/#jira-issue-panel) that uses the following fields:

* `title` displayed above panel.
  
* `allowMultple` allows to add multi instance of the panel.

* A corresponding [function module](https://developer.atlassian.com/platform/forge/manifest-reference/#function)
that implements the issue panel logic.

The function logic is implemented in those files:

  * main logic [src/app.jsx](./src/app.jsx)\

    * component used in app.jsx:

      * Alert: [src/alert/index.js](./src/alert/index.jsx)

      * Alerts feed: [src/alerts-feed/index.js](./src/alerts-feed/index.jsx)


The app's UI is implemented using these features:

- [`IssuePanel`](https://developer.atlassian.com/platform/forge/ui-kit-components/issue-panel) component.
- [`IssuePanelAction`](https://developer.atlassian.com/platform/forge/ui-kit-components/issue-panel-actionx) component. 
- [`Text`](https://developer.atlassian.com/platform/forge/ui-kit-components/text) component.
- [`Button`](https://developer.atlassian.com/platform/forge/ui-kit-components/button) component.
- [`ButtonSet`](https://developer.atlassian.com/platform/forge/ui-kit-components/button-set) component.
- [`Image`](https://developer.atlassian.com/platform/forge/ui-kit-components/image) component.
- [`ModalDialog`](https://developer.atlassian.com/platform/forge/ui-kit-components/modal-dialog) component.
- [`Form`](https://developer.atlassian.com/platform/forge/ui-kit-components/form) component.
- [`useState`](https://developer.atlassian.com/platform/forge/ui-hooks-reference/#usestate)
- [`useProductContext`](https://developer.atlassian.com/platform/forge/ui-hooks-reference/#useproductcontext)

The app is also using [Web trigger](https://developer.atlassian.com/platform/forge/manifest-reference/#web-trigger) module

Both UI and web trigger are using utility functions [src/utils.js](./src/utils.js)

## Contributions

Contributions to Issue Alerting are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details.

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
